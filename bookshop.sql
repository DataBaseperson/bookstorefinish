/*
 Navicat MySQL Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 50717
 Source Host           : localhost:3306
 Source Schema         : bookshop

 Target Server Type    : MySQL
 Target Server Version : 50717
 File Encoding         : 65001

 Date: 24/11/2021 17:39:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for book
-- ----------------------------
DROP TABLE IF EXISTS `book`;
CREATE TABLE `book`  (
  `bookid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `bookname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `publisher` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pubdate` datetime NULL DEFAULT NULL,
  `price` float(10, 2) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `quantity` int(255) NULL DEFAULT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`bookid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of book
-- ----------------------------
INSERT INTO `book` VALUES (' 001', ' web编程基础', ' 樊月华', ' 清华大学出版社', '2021-11-12 15:00:07', 33.00, ' web编程基本内容', ' 计算机', 100, 'web.jpg');
INSERT INTO `book` VALUES (' 002', 'c程序设计', ' 谭浩强', ' 清华大学出版社', '2021-11-10 15:17:47', 24.50, ' C语言基本内容', ' 计算机', 14, 'c.jpg');

-- ----------------------------
-- Table structure for bookcart
-- ----------------------------
DROP TABLE IF EXISTS `bookcart`;
CREATE TABLE `bookcart`  (
  `cartid` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bookid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bookname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `price` float(10, 2) NULL DEFAULT NULL,
  `booknum` int(20) NULL DEFAULT NULL,
  `bookcount` float(10, 2) NULL DEFAULT NULL,
  `picture` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`cartid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 199 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of bookcart
-- ----------------------------
INSERT INTO `bookcart` VALUES (148, '2', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (149, '2', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (150, '2', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (151, '2', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (152, '2', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (158, '2', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (187, '1', ' 002', 'c程序设计', 24.50, 1, 24.50, 'c.jpg');
INSERT INTO `bookcart` VALUES (190, '1', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (191, '1', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (192, '1', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');
INSERT INTO `bookcart` VALUES (193, '1', ' 002', 'c程序设计', 24.50, 1, 24.50, 'c.jpg');
INSERT INTO `bookcart` VALUES (194, '1', ' 002', 'c程序设计', 24.50, 1, 24.50, 'c.jpg');
INSERT INTO `bookcart` VALUES (195, '1', ' 002', 'c程序设计', 24.50, 1, 24.50, 'c.jpg');
INSERT INTO `bookcart` VALUES (196, '2', ' 002', 'c程序设计', 24.50, 1, 24.50, 'c.jpg');
INSERT INTO `bookcart` VALUES (197, '1', ' 002', 'c程序设计', 24.50, 1, 24.50, 'c.jpg');
INSERT INTO `bookcart` VALUES (198, '1', ' 001', ' web编程基础', 33.00, 1, 33.00, 'web.jpg');

-- ----------------------------
-- Table structure for booktable
-- ----------------------------
DROP TABLE IF EXISTS `booktable`;
CREATE TABLE `booktable`  (
  `bookid` int(255) NOT NULL AUTO_INCREMENT,
  `bookname` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `author` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `publisher` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `pubdate` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `price` char(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `quantity` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`bookid`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of booktable
-- ----------------------------
INSERT INTO `booktable` VALUES (1, '计算机网络教程', '谢希仁', '人民邮电出版社', '2012-8-1', '32', '计算机', '100');
INSERT INTO `booktable` VALUES (2, 'web技术应用基础', '樊月华', '清华大学出版社', '2013-9-3', '49', '计算机', '100');
INSERT INTO `booktable` VALUES (3, 'c程序设计', '谭浩强', '清华大学出版社', '2011-1-2', '50', '计算机', '100');
INSERT INTO `booktable` VALUES (5, '数据库原理与应用', '徐雨霞', '重庆工业出版社', '2011-4-2', '49', '计算机', '100');

-- ----------------------------
-- Table structure for employee
-- ----------------------------
DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee`  (
  `employeeid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `task` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`employeeid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of employee
-- ----------------------------
INSERT INTO `employee` VALUES ('001', 'zxc', '123456', '男', '呼和浩特', '2580647331@qq.com', '123456789', '管理系统');

-- ----------------------------
-- Table structure for notes
-- ----------------------------
DROP TABLE IF EXISTS `notes`;
CREATE TABLE `notes`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `subject` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `datel` datetime NULL DEFAULT NULL,
  `context` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `employeeid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `date2` datetime NULL DEFAULT NULL,
  `advice` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 8 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of notes
-- ----------------------------
INSERT INTO `notes` VALUES (3, '1', '书', '2021-11-24 17:23:18', 'asdfasdfasdfa', NULL, NULL, NULL);
INSERT INTO `notes` VALUES (4, '1', '书', '2021-11-24 17:24:00', '算法撒旦发射点发射点发撒', NULL, NULL, NULL);
INSERT INTO `notes` VALUES (5, '1', '书', '2021-11-24 17:33:56', '啊手动阀手动阀手动阀手动阀asdfas\r\nadfasdfa', NULL, NULL, NULL);
INSERT INTO `notes` VALUES (6, '1', '阿斯顿发射点发射点发射点', '2021-11-24 17:34:07', '阿斯顿发射点发射点发射点', NULL, NULL, NULL);
INSERT INTO `notes` VALUES (7, '1', '啊手动阀手动阀a', '2021-11-24 17:36:08', '啊手动阀手动阀手动阀', NULL, NULL, NULL);

-- ----------------------------
-- Table structure for orderdetail
-- ----------------------------
DROP TABLE IF EXISTS `orderdetail`;
CREATE TABLE `orderdetail`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `orderid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bookid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `bookname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `unitprice` float(10, 2) NULL DEFAULT NULL,
  `ordernum` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orderdetail
-- ----------------------------
INSERT INTO `orderdetail` VALUES (1, 'SF1637736046428', ' 001', ' web编程基础', 33.00, 4);
INSERT INTO `orderdetail` VALUES (2, 'SF1637736046428', ' 002', 'c程序设计', 24.50, 4);
INSERT INTO `orderdetail` VALUES (3, 'SF1637736065530', ' 001', ' web编程基础', 33.00, 4);
INSERT INTO `orderdetail` VALUES (4, 'SF1637736065530', ' 002', 'c程序设计', 24.50, 4);
INSERT INTO `orderdetail` VALUES (5, 'SF1637736345833', ' 001', ' web编程基础', 33.00, 4);
INSERT INTO `orderdetail` VALUES (6, 'SF1637736345833', ' 002', 'c程序设计', 24.50, 4);
INSERT INTO `orderdetail` VALUES (7, 'SF1637740724886', ' 001', ' web编程基础', 33.00, 6);
INSERT INTO `orderdetail` VALUES (8, 'SF1637740724886', ' 002', 'c程序设计', 24.50, 1);

-- ----------------------------
-- Table structure for orderform
-- ----------------------------
DROP TABLE IF EXISTS `orderform`;
CREATE TABLE `orderform`  (
  `orderid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `orderdate` datetime NULL DEFAULT NULL,
  `userid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `totalnum` int(11) NULL DEFAULT NULL,
  `payment` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deliver` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `receiver` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `postcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `state` int(10) UNSIGNED ZEROFILL NULL DEFAULT NULL,
  PRIMARY KEY (`orderid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of orderform
-- ----------------------------
INSERT INTO `orderform` VALUES ('SF1637736046428', '2021-11-24 14:40:46', '1', 230, '支付宝', '顺丰快递', 'zhengxuecheng', '无', '15714848131', '013450', NULL);
INSERT INTO `orderform` VALUES ('SF1637736065530', '2021-11-24 14:41:05', '1', 230, '支付宝', '顺丰快递', 'zhengxuecheng', '无', '15714848131', '013450', NULL);
INSERT INTO `orderform` VALUES ('SF1637736345833', '2021-11-24 14:45:45', '1', 230, '支付宝', '顺丰快递', 'zhengxuecheng', '无', '15714848131', '013450', NULL);
INSERT INTO `orderform` VALUES ('SF1637740724886', '2021-11-24 15:58:44', '2', 223, '支付宝', '顺丰快递', 'zhengxuecheng', '无', '15714848131', '013450', NULL);

-- ----------------------------
-- Table structure for publisher
-- ----------------------------
DROP TABLE IF EXISTS `publisher`;
CREATE TABLE `publisher`  (
  `publisherid` int(255) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `linkman` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `website` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`publisherid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of publisher
-- ----------------------------

-- ----------------------------
-- Table structure for userinfo
-- ----------------------------
DROP TABLE IF EXISTS `userinfo`;
CREATE TABLE `userinfo`  (
  `userid` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `gender` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `phone` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `postcode` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`userid`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of userinfo
-- ----------------------------
INSERT INTO `userinfo` VALUES ('1', 'zxc', '123456', 'asd', 'asd', 'asdasd', 'asdasd', 'asdasd');
INSERT INTO `userinfo` VALUES ('12', '张三', '123', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('1234', '张三', '123', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('2', '张三', 'qwe', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('213', '阿松大', '123', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('3', '张三', 'qwe', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('4', '张三', 'qwe', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('5', '张三', 'qwe', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('7', '张三', '123', '男', '无', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('王五', '张三', '123456', '男', '去问驱蚊器围墙', '1234657890@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('董云龙', '张三', '123', '男', '123', '2580647331@qq.com', '15714848131', '013450');
INSERT INTO `userinfo` VALUES ('赵六', '张三', '123', '男', '撒旦发射点', '1234657890@qq.com', '15714848131', '013450');

SET FOREIGN_KEY_CHECKS = 1;
