
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.*" %>

<html>
  <head>
    <title>网上书城</title>
    <script language="JavaScript">
      function setsearch() {
        document.getElementById("sear").value="";
      }
      function clicknum() {
          if(document.getElementById("num").value==0){
            alert("数量不能为0,请重新输入");
            window.event.returnValue=false;
            document.searchform.num.focus()
          }


      }

    </script>
  </head>
  <body >
  <div align="center" >
    <%@include file="top.jsp"%>
  </div>
  <div   style=" position: absolute;right:150px;float: right">
<%--    <a href="login.jsp" name="log">登录</a>&nbsp;&nbsp;&nbsp;&nbsp;<a href="register.jsp">注册</a>--%>

    <%
      String exit=request.getParameter("exit");
      if(exit!=null){
        session.removeAttribute("userid");
        session.invalidate();
      }

      String userid = (String) request.getSession().getAttribute("userid");
//      String exit=null;
//      exit = request.getParameter("exit");


      if (userid != null) {
        out.print(userid+"你好"+"<a href='index.jsp?exit=exit'>退出</a>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"<a href='shopingcart.jsp'>购物车</a>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"<a href='showorder.jsp'>查看订单</a>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"<a href='message.jsp'>留言版</a>");
      }
      else{
        out.print("<a href='login.jsp'>登录</a>"+"&nbsp;&nbsp;&nbsp;&nbsp;"+"<a href='register.jsp'>注册</a>");
      }

    %>



      
    
    

    <form action="search.jsp" method="post">
      <input type="text" name="search"  id= "sear"value="请输入书名关键词搜索" onclick="setsearch()">
      <input type="submit" value="搜索">
    </form>
<%--    <a href="shopingcart.jsp?userid=<%=userid%>">查看购物车</a>--%>
  </div>
  <%

    Class.forName("com.mysql.jdbc.Driver");
    Connection conn=DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop","root","123456");
    Statement stmt=conn.createStatement();
    try{
      ResultSet rs; //建立ResultSet(结果集)对象
      rs=stmt.executeQuery("SELECT * FROM book"); //执行SQL语句
  %>

    <div  align="center" style="position:relative;top:100px;">
    <table border="2" >
      <tr bgcolor=silver><br>
        <td>图片</td><td>书号</td><td>书名</td><td>作者</td><td>出版社</td>
        <td>出版日期</td><td>单价</td><td>内容简介</td><td>类型</td><td>加入购物车</td>
      </tr>
      <%
        //利用while循环将数据表中的记录列出
        while (rs.next()){
      %>

      <tr>
        <td><font size=1><img src="images/<%=rs.getString("picture")%>" width="100" height="100"></font></td>
        <td><font size=2><%= rs.getString("bookid") %></font></td>
        <td><font size=2><%= rs.getString("bookname") %></font></td>
        <td><font size=2><%= rs.getString("author") %></font></td>
        <td><font size=2><%= rs.getString("publisher") %></font></td>
        <td><font size=2><%= rs.getString("pubdate") %></font></td>
        <td><font size=2><%= rs.getString("price") %></font></td>
        <td><font size=2><%= rs.getString("content") %></font></td>
        <td><font size=2><%= rs.getString("type") %></font></td>


<%--        <td><font><input type="text" name="num"><a href="addshopingcart.jsp?bookid=<%=rs.getString("bookid")%>"><input type="submit" name="subnum" value="加入购物车"></a></font></td>--%>

<%--        <td><form name="searchform" action="addshopingcart.jsp?bookid=<%=rs.getString("bookid")%>&userid=<%=userid%>" method="post"><input type="text" name="num" value="1" id="num"><input type="submit" value="加入购物车" onclick="clicknum()"></form></td>--%>
        <td><button><a href="addshopingcart.jsp?bookid=<%=rs.getString("bookid")%>&userid=<%=userid%>">加入购物车</a></button></td>
      </tr>


      <%
          }
          rs.close(); //关闭ResultSet对象
        }
        catch(Exception e){
          out.println(e.getMessage());
        }
        stmt.close(); //关闭Statement对象
        conn.close();  //关闭Connection对象
      %>
    </table>
  </div>
  </body>
</html>
