<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="java.sql.*" %>
<%@ page import="java.net.URLEncoder" %>


<html>
<%
    request.setCharacterEncoding("utf-8");
    String str=request.getParameter("str");
   if(str!=null&&str.equals("register")) {
       String userid = request.getParameter("userid");
       String password = request.getParameter("password");
       String username = request.getParameter("username");
       String gender = request.getParameter("gender");
       String address = request.getParameter("address");
       String phone = request.getParameter("phone");
       String postcode = request.getParameter("postcode");
       String email = request.getParameter("email");

       Class.forName("com.mysql.jdbc.Driver");
       Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop", "root", "123456");
       Statement stmt = conn.createStatement();
//
       ResultSet rs;
       try {
//        if(!userid.equals("")){
           stmt.executeUpdate("insert into userinfo(userid,password,username,gender,address,phone,postcode,email) " +
                   "values('" + userid + "','" + password + "','" + username + "','" + gender + "','" + address + "','" + phone + "','" + postcode + "','" + email + "')");
//        }
       } catch (Exception e) {
           e.printStackTrace();
           out.print("<center><h1><font color='red'>用户名已存在</font></h1></center>");
           String error="注册失败，用户名已存在";
           error= URLEncoder.encode(error,"utf-8");
           response.sendRedirect("error.jsp?error="+error);

       }
       stmt.close(); //关闭Statement对象
       conn.close();  //关闭Connection对象
   }
%>
<head>
    <title>Title</title>
    <meta charset="UTF-8">
    <script language="JavaScript">
        function CheckSubmit()
        {
            if( document.registerform.userid.value == "" )
            { alert("请输入用户名!"); document.registerform.userid.focus(); return false; }
            if( document.registerform.password.value == "" )
            { alert("请输入密码!"); document.registerform.password.focus(); return false; }
            if( document.registerform.password2.value == "" )
            { alert("请验证密码!"); document.registerform.password2.focus(); return false; }
            if( document.registerform.password2.value != document.registerform.password.value  )
            { alert("密码验证出错!"); document.registerform.password.focus(); return false; }
            if( document.registerform.username.value == "" )
            { alert("请输入真实姓名!"); document.registerform.username.focus(); return false; }
            if( document.registerform.address.value == "" )
            { alert("请输入住址!"); document.registerform.address.focus(); return false; }
            if( document.registerform.postcode.value == "" )
            { alert("请输入邮编!"); document.registerform.postcode.focus(); return false; }
            if( document.registerform.phone.value == "" )
            { alert("请输入联系电话!"); document.registerform.phone.focus(); return false; }
            return true;
        }
    </script>
</head>
<body background="images/bg.png">
<%
    if(str==null){
%>
<div  style="position:relative;left:600px;top:100px;width:500px;height:50px;float: left">
<form name="registerform" action="register.jsp?str=register" method="post" >
    <tr>
        <td colspan="2">&nbsp;&nbsp;以下资料请如实填写，以保证正确发货。这些资料未经客户允许将只用于处理客户的订货信息，对外严格保密。（带
            <font color="red">*</font>号的为必填项，其它的可以不填）</td><br>
    </tr>
    <tr>
        <td >用户名</td><br>
        <td><input name="userid" type="text" size="20"><font color="red">*</font></td><br>
    </tr>
    <tr>
        <td>密码</td><br>
        <td><input name="password" type="password" size="20"><font color="red">*</font></td><br>
    </tr>
    <tr>
        <td>验证密码</td><br>
        <td><input name="password2" type="password" size="20"><font color="red">*</font></td><br>
    </tr>
    <tr>
        <td>真实姓名</td><br>
        <td><input name="username" type="text" size="20"><font color="red">*</font></td><br>
    </tr>
    <tr>
        <td>性别</td>
        <td>
            <select name="gender">
                <option value="男">男</option><br>
                <option value="女">女</option><br>
            </select>
        </td>
    </tr>
    <tr><br>
        <td>住址</td>
        <td><input name="address" type="text" size="20"><font color="red">*</font>(请您提供尽可能详细的地址)</td><br>
    </tr>
    <tr>
        <td>邮编</td><br>
        <td><input name="postcode" type="text" size="20"><font color="red">*</font></td><br>
    </tr>
    <tr>
        <td>联系电话</td><br>
        <td><input name="phone" type="text" size="20"><font color="red">*</font></td><br>
    </tr>
    <tr>
        <td>E-mail</td><br>
        <td><input name="email" type="text" size="20"></td><br>
    </tr>
    <tr>
        <td>

            <input name="submit" type="submit" value=" 注册新用户 " onClick="return CheckSubmit();">
            <input name="reset" type="reset" value=" 重新填写 "><br>
       </td>

    </tr>
<%--    <tr>--%>
<%--        <td>--%>
<%--            <a href="login.jsp"><font color="red">返回登录</font></a>--%>
<%--        </td>--%>
<%--    </tr>--%>
</form>
<%

//    rs=stmt.executeQuery("SELECT * FROM book");
//    if(rs.next())
    }else {%>
        <div style="position:relative;left:600px;top:100px;width:500px;height:50px;float: left">
            <tr>
                <td colspan="2">&nbsp;&nbsp;<font size="20px">恭喜,注册成功</font><a href="login.jsp">登陆</a>.</td>
            </tr>
        </div>

    <%}%>


</div>

</body>
</html>
