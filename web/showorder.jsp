<%@ page import="java.sql.*" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%
    Class.forName("com.mysql.jdbc.Driver");
    Connection conn= DriverManager.getConnection("jdbc:mysql://localhost:3306/bookshop","root","123456");
    Statement stmt=conn.createStatement();
    Statement statement=conn.createStatement();
    String userid = (String) request.getSession().getAttribute("userid");
    ResultSet rs;
    rs = stmt.executeQuery("select orderform.orderid,orderform.receiver,orderform.address,orderform.phone,orderform.postcode,group_concat(orderdetail.bookname) bookname,orderdetail.unitprice,sum(orderdetail.ordernum) from orderform left join orderdetail on orderform.orderid=orderdetail.orderid where orderform.userid='"+userid+"' group by orderform.orderid ");

%>

<div  align="center" style="position:relative;top:100px;">
    <p align="center">付款成功，您的订单信息如下：</p>
    <table border="2" >
        <tr bgcolor=silver><br>
            <td>书名</td><td>单价</td><td>数量</td>
            <td>定单编号</td><td>收货人姓名</td><td>地址</td><td>联系电话</td><td>邮政编码</td>
        </tr>
        <%
            //利用while循环将数据表中的记录列出
            while (rs.next()){
        %>

        <tr>
            <td><font size=2><%= rs.getString("bookname") %></font></td>
            <td><font size=2><%= rs.getString("orderdetail.unitprice") %></font></td>
            <td><font size=2><%= rs.getString("sum(orderdetail.ordernum)") %></font></td>
            <td><font size=2><%= rs.getString("orderid") %></font></td>
            <td><font size=2><%= rs.getString("receiver") %></font></td>
            <td><font size=2><%= rs.getString("address") %></font></td>
            <td><font size=2><%= rs.getString("phone") %></font></td>
            <td><font size=2><%= rs.getString("postcode") %></font></td>

        </tr>


        <%
            }

            rs.close();
            stmt.close();
            conn.close();

        %>
    </table>
</body>
</html>
